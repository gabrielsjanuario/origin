<?php

namespace estoque\Http\Controllers;

use estoque\Produto;
use Request;
use estoque\Http\Requests\ProdutosRequest;

class ProdutoController extends Controller
{
    public function index()
    {

        $produtos = Produto::all();
		return view('produto.listagem')->with('produtos', $produtos);

    }

    public function mostra($id)
    {

        $produto = Produto::find($id);

        if(empty($produto)){
            return "Esse produto não existe";
        }
            return view ('produto.detalhes')
            ->with('p',$produto);
    }

    public function novo()
    
    {
   
        return view ('produto.formulario');
    }


    public function adiciona(ProdutosRequest $request)
    {
        Produto::create($request->all());
        return redirect('/produtos')->withInput(Request::only('nome'));

    }

    public function edit($id){
        $produto = Produto::find($id);
        return view('produto.edit')->with('p', $produto);
    }

    public function update($id){
        $produto = Produto::find($id);
        $params = Request::all();// carrega informações digitadas na view
        $produto->update($params);

        return redirect()->action('ProdutoController@index')->withInput(Request::only('nomeAlt'));
    }

    public function ListaJson()
    {
        $produtos = Produto::all(); 
        return response()->json($produtos);

    }

    public function remove($id){
        $produto = Produto::find($id);
        $produto->delete();
        return redirect()
        ->action('ProdutoController@index');
       
    }

    public function relacao()
    {
        $produtos = Produto::all();

        return view('produto.relacao')
        ->with('produtos',$produtos);
    }
}
