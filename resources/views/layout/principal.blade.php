<html>
<head>
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
		<script href="/js/bootstrap.min.js" rel="stylesheet"></script>

	


    <title>Gerenciador de Residencia</title>
</head>
<body>

	<nav class="navbar navbar-default">
	  <div class="container-fluid">
		<img id="LOGO" src="/images/home_icon-icons.com_73532.png">    

		<div class="navbar-header">  
		  <a class="navbar-brand" href="/" style="font-size: 25px;">GERENCIADOR DE RESIDENCIA</a>
		</div>

	    <ul class="list-inline">
			<!-- <li class="list-inline-item"><a href="{{action('ProdutoController@relacao')}}">Comprar</a></li>
	      <li class="list-inline-item"><a href="{{action('ProdutoController@index')}}">Lista</a></li>
	      <li class="list-inline-item"><a href="{{action('ProdutoController@novo')}}">Novo</a></li> -->
				<li class="list-inline-item"> <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">
           {{ __('Sair') }}
           </a>
           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
				</li>
	    </ul>

	  </div>
	</nav>

    @yield('conteudo')

	<footer class="footer">
	    <p>©Smart House</p>
	</footer>
    
</body>
</html>