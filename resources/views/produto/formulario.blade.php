@extends('layout.principal')

@section('conteudo')

<div class="container">
    <h1>Cadastrar Lampada</h1>

    <form action="/produtos/update" method="post">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
        <div class="form-group">
            <label>Comodo</label>
                <input name="nome" class="form-control">
        </div>
        <div class="form-group">
            <label>Watt/hora</label>
            <input name="descricao" class="form-control">
        </div>

        <div class="form-group">
            <label>Quantidade</label>
            <input name="quantidade" class="form-control" type="number"/>
        </div>
            <button type="submit" class="btn btn-primary btn-block">Salvar</button>
    </form>

</div>

@stop