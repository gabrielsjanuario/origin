@extends('layout.principal')

@section('conteudo')
@if(old('nome'))
<div class="alert alert-success"> O produto {{old('nome')}} foi adicionado com sucesso
</div>
@endif

 @if(empty($produtos))
  <div class="alert alert-danger">
    Você não tem nenhum produto cadastrado.
  </div>
 
 @else
  <h1>Produtos</h1>
  <table class="table table-striped table-bordered table-hover">
    <tr>
      <th>Nome</th>
      <th>Valor</th>
      <th>Descrição</th>
      <th>Qtd</th>
    </tr>
    @foreach ($produtos as $p)
    @if ($p->valor >=1000 && $p->quantidade <=2 || $p->valor <=999 && $p->quantidade <=5 )
    <tr class="{{'danger'}}">
      <td> {{$p->nome}} </td>
      <td> {{$p->valor}} </td>
      <td> {{$p->descricao}} </td>
      <td> {{$p->quantidade}} </td>
      <td> 
        <a href="{{action('ProdutoController@mostra', $p->id)}}">
          <span class="glyphicon glyphicon-search"></span>
        </a>
      </td>
      <td> 
        <a href="{{action('ProdutoController@remove', $p->id)}}">
          <span class="glyphicon glyphicon-trash"></span>
        </a>
      </td>
    </tr>
    @endif
    @endforeach
  </table>
 @endif
@stop