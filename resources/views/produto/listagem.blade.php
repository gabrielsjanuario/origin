@extends('layout.principal')

@section('conteudo')

<div class="container">
<h1>Lâmpadas</h1>

  <table class="table table-striped table-bordered table-hover">
    <tr>
      <td><strong>Quarto Suite</strong> </td>
      <td> 
        <label class="switch" style="margin:0px;">
          <input id="luzquarto" type="checkbox" class="switch-input">
          <span class="switch-label" data-on="On" data-off="Off"></span>
          <span class="switch-handle"></span>
        </label>
      </td>
    </tr>

    <tr>
      <td><strong> Quarto 1</strong> </td>
      <td> 
        <label class="switch" style="margin:0px;">
          <input id="luzquarto1" type="checkbox" class="switch-input">
          <span class="switch-label" data-on="On" data-off="Off"></span>
          <span class="switch-handle"></span>
        </label>
      </td>
    </tr>

    <tr>
      <td><strong> Sala </strong> </td>
      <td> 
        <label class="switch" style="margin:0px;">
          <input type="checkbox" class="switch-input">
          <span class="switch-label" data-on="On" data-off="Off"></span>
          <span class="switch-handle"></span>
        </label>
      </td>
    </tr>

    <tr>
      <td><strong> Cozinha </strong> </td>
      <td> 
        <label class="switch" style="margin:0px;">
          <input type="checkbox" class="switch-input">
          <span class="switch-label" data-on="On" data-off="Off"></span>
          <span class="switch-handle"></span>
        </label>
      </td>
    </tr>

    <tr>
      <td> <strong> Quintal </strong> </td>
      <td> 
        <label class="switch" style="margin:0px;">
          <input type="checkbox" class="switch-input">
          <span class="switch-label" data-on="On" data-off="Off"></span>
          <span class="switch-handle"></span>
        </label>
      </td>
    </tr>

    <tr>
      <td><strong> lavanderia </strong> </td>
      <td> 
        <label class="switch" style="margin:0px;">
          <input type="checkbox" class="switch-input">
          <span class="switch-label" data-on="On" data-off="Off"></span>
          <span class="switch-handle"></span>
        </label>
      </td>
    </tr>

  </table>
</div>

@stop

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script>

$(document).ready(function(){
  
  //LUZ SUITE
  $('#luzquarto').click(function() {
    if ($(this).prop("checked") == true) {
          alert('Luz Suite Ligada');
    } else if ($(this).prop("checked") == false) {
          alert('Luz Suite Desligada');
    }
  });

  //LUZ QUARTO 1
  $('#luzquarto1').click(function() {
    if ($(this).prop("checked") == true) {
          alert('Luz Quarto1 Ligada');
    } else if ($(this).prop("checked") == false) {
          alert('Luz Quarto1 Desligada');
    }
  });

});
</script>
