@extends('layout.principal')

@section('conteudo')

<h1>Detalhes do produto: {{$p->nome}} </h1>

<ul>
<li>
    <b>Nome:</b> {{$p->nome}} 
  </li>
  <li>
    <b>Valor:</b> R$ {{$p->valor}} 
  </li>
  <li>
    <b>Descrição:</b> {{$p->descricao or 'nenhuma descrição informada'}} 
  </li>
  <li>
    <b>Quantidade em estoque:</b> {{$p->quantidade}}
  </li>

</ul>

<a href="{{action('ProdutoController@edit', $p->id)}}"><button type="button" class="btn btn-primary">Editar</button></a>




@stop