<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
Route::get('/' , 'MenuController@index')->name('index.php');
Route::get('/lampadas' , 'ProdutoController@index');
Route::get('/portao' , 'PortaoController@index');
Route::get('/produtos/mostra/{id}','ProdutoController@mostra')->where('id', '[0-9]+');
Route::get('/lampadas/novo', 'ProdutoController@novo');
Route::post('/produtos/adiciona', 'ProdutoController@adiciona');
Route::get('/produtos/json', 'ProdutoController@ListaJson');
Route::get('/produtos/edit/{id}','ProdutoController@edit');
Route::post('/produtos/update/{id}','ProdutoController@update');
Route::get('/produtos/remove/{id}', 'ProdutoController@remove');
Route::get('/produtos/comprar','ProdutoController@relacao');
});


Auth::routes();


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


